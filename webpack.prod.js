const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

moduel.exports = merge(common, {
  mode: 'production',
  plugins: [
    new UglifyJsPlugin()
  ]
});